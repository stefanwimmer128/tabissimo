import { addLink, removeLink, updateLink, reorderLink } from "./actions";
import { reducer } from "./reducer";

describe("links/reducer()", () => {
  it("should add new links", () => {
    expect(reducer([], addLink())).toEqual([{ url: "https://" }]);
    expect(
      reducer([{ url: "https://tabissimo.io/" }], { type: "ADD_LINK" }),
    ).toEqual([{ url: "https://tabissimo.io/" }, { url: "https://" }]);
  });

  it("should remove links", () => {
    expect(
      reducer(
        [
          { url: "https://tabissimo.io/" },
          { url: "https://tabissimo.io/about.html" },
        ],
        removeLink(0),
      ),
    ).toEqual([{ url: "https://tabissimo.io/about.html" }]);
  });

  it("should update links", () => {
    expect(
      reducer(
        [
          { url: "https://tabissimo.io/" },
          { url: "https://tabissimo.io/about.html" },
        ],
        updateLink(0, { name: "Tabissimo", url: "https://tabissimo.io/" }),
      ),
    ).toEqual([
      { name: "Tabissimo", url: "https://tabissimo.io/" },
      { url: "https://tabissimo.io/about.html" },
    ]);
  });

  it("should reorder links", () => {
    expect(
      reducer(
        [
          { url: "https://tabissimo.io/" },
          { url: "https://tabissimo.io/about.html" },
          { url: "https://codeberg.org/stefanwimmer128/tabissimo/issues" },
        ],
        reorderLink(1, 0),
      ),
    ).toEqual([
      { url: "https://tabissimo.io/about.html" },
      { url: "https://tabissimo.io/" },
      { url: "https://codeberg.org/stefanwimmer128/tabissimo/issues" },
    ]);

    expect(
      reducer(
        [
          { url: "https://tabissimo.io/" },
          { url: "https://tabissimo.io/about.html" },
          { url: "https://codeberg.org/stefanwimmer128/tabissimo/issues" },
        ],
        reorderLink(1, 2),
      ),
    ).toEqual([
      { url: "https://tabissimo.io/" },
      { url: "https://codeberg.org/stefanwimmer128/tabissimo/issues" },
      { url: "https://tabissimo.io/about.html" },
    ]);
  });

  it("should throw on unknown action", () => {
    expect(() => reducer([], { type: "UNKNOWN" } as any)).toThrow();
  });
});
