import { API } from "../../types";

export type Data = {
  angle: number;
  from: string;
  to: string;
  type: "linear-gradient" | "radial-gradient";
};

export type Props = API<Data>;

export const defaultData: Data = {
  angle: 0,
  from: "#00b7ff",
  to: "#9b59b6",
  type: "linear-gradient",
};
