import React from "react";
import "./Logo.css";
import tabissimoLogo from "./tabissimo.svg";

const Logo: React.FC = () => (
  <h1 className="Logo">
    <i dangerouslySetInnerHTML={{ __html: tabissimoLogo }} />
  </h1>
);

export default Logo;
